package pl.mypaint.affineOperations;

import java.awt.Point;
import java.awt.geom.AffineTransform;
import java.awt.image.AffineTransformOp;
import java.awt.image.BufferedImage;

public class AffineRotation {
	public AffineRotation() {
	}

	public static BufferedImage rotateImage(BufferedImage image, int degree) {
		int w = image.getWidth();
		int h = image.getHeight();

		AffineTransform t = new AffineTransform();
		double ang = Math.toRadians(degree);
		t.setToRotation(ang, w / 2d, h / 2d);

		Point[] pointsTab = {new Point(0, 0), new Point(w, 0), new Point(w, h),
				new Point(0, h)};

		t.transform(pointsTab, 0, pointsTab, 0, 4);

		Point min = new Point(pointsTab[0]);
		Point max = new Point(pointsTab[0]);
		for (int i = 1, n = pointsTab.length; i < n; i++) {
			Point p = pointsTab[i];
			double pX = p.getX(), pY = p.getY();

			if (pX < min.getX()) {
				min.setLocation(pX, min.getY());
			}
			if (pX > max.getX()) {
				max.setLocation(pX, max.getY());
			}

			if (pY < min.getY()) {
				min.setLocation(min.getX(), pY);
			}
			if (pY > max.getY()) {
				max.setLocation(max.getX(), pY);
			}
		}

		w = (int) (max.getX() - min.getX());
		h = (int) (max.getY() - min.getY());

		double tx = min.getX();
		double ty = min.getY();

		AffineTransform translation = new AffineTransform();
		translation.translate(-tx, -ty);
		t.preConcatenate(translation);

		AffineTransformOp op = new AffineTransformOp(t, null);

		return op.filter(image, null);
	}
}
