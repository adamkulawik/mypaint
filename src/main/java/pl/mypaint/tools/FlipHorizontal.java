package pl.mypaint.tools;

import java.awt.Color;
import java.awt.Point;
import java.util.List;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.FlipRotateAction;
import pl.mypaint.affineOperations.AffineHorizontalFlip;
import pl.mypaint.gui.MyImage;

public class FlipHorizontal implements Tool {

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		myImage.setDisplayedImage(AffineHorizontalFlip.flipHorizontal(myImage
				.getDisplayedImage()));
	}

	@Override
	public DrawAction getAction() {
		return new FlipRotateAction(this);
	}
}
