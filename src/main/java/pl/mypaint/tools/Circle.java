package pl.mypaint.tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.util.List;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.TwoPointAction;
import pl.mypaint.gui.MyImage;

public class Circle implements Tool {

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		Graphics g = myImage.getDisplayedImage().getGraphics();

		if (points.size() < 2) {
			return;
		}

		int radius = countRadius(points.get(0), points.get(1));

		g.setColor(color);
		g.drawOval(points.get(0).x - radius, points.get(0).y - radius,
				2 * radius, 2 * radius);
	}

	@Override
	public DrawAction getAction() {
		return new TwoPointAction(this);
	}

	public static int countRadius(Point center, Point pointOnCircle) {
		return new Double(Math.sqrt(Math.pow(
				Math.abs(center.x - pointOnCircle.x), 2)
				+ Math.pow(Math.abs(center.y - pointOnCircle.y), 2)))
				.intValue();
	}
}
