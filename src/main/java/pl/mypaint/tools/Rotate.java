package pl.mypaint.tools;

import java.awt.Color;
import java.awt.Point;
import java.util.List;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.FlipRotateAction;
import pl.mypaint.affineOperations.AffineRotation;
import pl.mypaint.gui.MyImage;

public class Rotate implements Tool {
	int value;

	public Rotate() {
	}

	public Rotate(int value) {
		this.value = value;
	}

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		myImage.setDisplayedImage(AffineRotation.rotateImage(
				myImage.getDisplayedImage(), value));
	}

	@Override
	public DrawAction getAction() {
		return new FlipRotateAction(this);
	}
}
