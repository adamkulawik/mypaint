package pl.mypaint.tools;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.MultiPointAction;
import pl.mypaint.gui.MyImage;

public class Rubber implements Tool {
	public Rubber() {
	}

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		BufferedImage image = myImage.getDisplayedImage();

		Graphics2D g2 = (Graphics2D) image.getGraphics();

		if (points.size() > 0) {
			Point prevPoint = points.get(0);

			g2.setColor(Color.WHITE);
			g2.setStroke(new BasicStroke(5));

			for (Point point : points) {
				g2.drawLine(prevPoint.x, prevPoint.y, point.x, point.y);
				prevPoint = point;
			}
		}
	}

	@Override
	public DrawAction getAction() {
		return new MultiPointAction(this);
	}
}
