package pl.mypaint.tools;

import java.awt.Color;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.SinglePointAction;
import pl.mypaint.gui.MyImage;

public class Bucket implements Tool {

	public Bucket() {
	}

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		Point point = points.get(0);
		BufferedImage image = myImage.getDisplayedImage();

		fill(image, point.x, point.y,
				new Color(image.getRGB(point.x, point.y)), color);
	}

	public void fill(BufferedImage image, int x, int y, Color oldColor,
			Color newColor) {
		boolean[][] painted = new boolean[image.getWidth()][image.getHeight()];

		Queue<Point> queue = new LinkedList<Point>();
		queue.add(new Point(x, y));

		int width = image.getWidth();
		int height = image.getHeight();

		while (!queue.isEmpty()) {
			Point pointForProcess = queue.remove();

			if ((pointForProcess.x >= 0) && (pointForProcess.x < width)
					&& (pointForProcess.y >= 0) && (pointForProcess.y < height)) {

				if (!painted[pointForProcess.x][pointForProcess.y]
						&& new Color(image.getRGB(pointForProcess.x,
								pointForProcess.y)).equals(oldColor)) {

					painted[pointForProcess.x][pointForProcess.y] = true;

					image.setRGB(pointForProcess.x, pointForProcess.y,
							newColor.getRGB());

					queue.add(new Point(pointForProcess.x + 1,
							pointForProcess.y));
					queue.add(new Point(pointForProcess.x - 1,
							pointForProcess.y));
					queue.add(new Point(pointForProcess.x,
							pointForProcess.y + 1));
					queue.add(new Point(pointForProcess.x,
							pointForProcess.y - 1));
				}
			}
		}
	}

	@Override
	public DrawAction getAction() {
		return new SinglePointAction(this);
	}
}
