package pl.mypaint.tools;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.MultiPointAction;
import pl.mypaint.gui.MyImage;

public class Pencil implements Tool {

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		BufferedImage image = myImage.getDisplayedImage();
		Graphics g = image.getGraphics();

		if (points.size() > 0) {
			Point prevPoint = points.get(0);

			g.setColor(color);

			for (Point point : points) {
				g.drawLine(prevPoint.x, prevPoint.y, point.x, point.y);
				prevPoint = point;
			}
		}
	}

	@Override
	public DrawAction getAction() {
		return new MultiPointAction(this);
	}
}
