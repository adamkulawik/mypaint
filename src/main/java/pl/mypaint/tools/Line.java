package pl.mypaint.tools;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.actions.TwoPointAction;
import pl.mypaint.gui.MyImage;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.List;

public class Line implements Tool {

	@Override
	public void draw(MyImage myImage, List<Point> points, Color color) {
		if (points.size() < 2) {
			return;
		}

		BufferedImage image = myImage.getDisplayedImage();
		Graphics g = image.getGraphics();

		g.setColor(color);
		g.drawLine(points.get(0).x, points.get(0).y, points.get(1).x,
				points.get(1).y);
	}

	@Override
	public DrawAction getAction() {
		return new TwoPointAction(this);
	}
}
