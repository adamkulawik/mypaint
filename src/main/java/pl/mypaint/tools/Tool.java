package pl.mypaint.tools;

import java.awt.Color;
import java.awt.Point;
import java.util.List;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.gui.MyImage;

public interface Tool {
	public abstract void draw(MyImage myImage, List<Point> points, Color color);
	public abstract DrawAction getAction();
}
