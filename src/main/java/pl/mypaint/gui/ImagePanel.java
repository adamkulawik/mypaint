package pl.mypaint.gui;

import pl.mypaint.actions.DrawAction;
import pl.mypaint.tools.Pencil;
import pl.mypaint.tools.Tool;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

public class ImagePanel extends JPanel {
	private static final long serialVersionUID = -4556856446371748562L;

	private MyPaint parent;
	private MyImage myImage;
	private Color color = new Color(0, 0, 0);
	private Tool activeTool = new Pencil();
	private DrawAction drawAction;
	private boolean mouseDown = false;
	private boolean doFullDraw;

	public ImagePanel(MyPaint parent) {
		this.parent = parent;
		this.setBackground(Color.WHITE);
		this.setFixedSize(new Dimension(1024, 768));
		this.addMouseListener(new MouseActionListener());
		this.addMouseMotionListener(new MouseActionListener());

		createImages(this.getSize());

		color = Color.BLACK;

		refresh();
	}

	public ImagePanel(Dimension d) {
		this.setSize(d);
		this.createImages(d);
		color = Color.BLACK;
	}

	public void createImages(Dimension d) {
		doFullDraw = true;
		myImage = new MyImage(d);

	}

	public void newImagePanel(Dimension d) {
		activeTool = new Pencil();
		doFullDraw = true;
		this.setBackground(Color.WHITE);
		this.setFixedSize(d);
		this.createImages(d);
		parent.getToolBar().resetButtons();
		refresh();
	}

	private void setFixedSize(Dimension d) {
		this.setMinimumSize(d);
		this.setMaximumSize(d);
		this.setPreferredSize(d);
		this.setSize(d);
	}

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public Tool getActiveTool() {
		return activeTool;
	}

	public void setActiveTool(Tool activeTool) {
		this.activeTool = activeTool;
	}

	public MyImage getMyImage() {
		return myImage;
	}

	public void setMyImage(MyImage image) {
		this.myImage = image;
	}

	public BufferedImage getDisplayedImage() {
		return myImage.getDisplayedImage();
	}

	public void setDisplayedImage(BufferedImage displayedImage) {
		this.myImage.setDisplayedImage(displayedImage);
	}

	public void setImage(BufferedImage image) {
		this.setFixedSize(new Dimension(image.getWidth(), image.getHeight()));
		doFullDraw = true;
		myImage.setOriginalImage(image);
		parent.getToolBar().resetButtons();
		this.activeTool = new Pencil();
		refresh();
	}

	public void undo() {
		myImage.undo();
		doFullDraw = true;
		refresh();
	}

	public void refresh() {
		if (doFullDraw) {
			myImage.redraw();
			doFullDraw = false;
		} else {
			if (!mouseDown) {
				myImage.draw();
			}
		}

		BufferedImage displayedImage = myImage.getDisplayedImage();

		if ((displayedImage.getWidth() != this.getWidth())
				|| (displayedImage.getHeight() != this.getHeight())) {
			this.setFixedSize(new Dimension(displayedImage.getWidth(),
					displayedImage.getHeight()));
		} else {
			repaint();
		}
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);

		BufferedImage displayedImage = myImage.getDisplayedImage();

		BufferedImage bufferImage = new BufferedImage(
				displayedImage.getWidth(), displayedImage.getHeight(),
				displayedImage.getType());

		bufferImage.getGraphics().drawImage(displayedImage, 0, 0, null);

		if (drawAction != null) {
			drawAction.draw(myImage);
		}

		g.drawImage(displayedImage, 0, 0, null);

		displayedImage.getGraphics().drawImage(bufferImage, 0, 0, null);
	}

	private class MouseActionListener
			implements
				MouseListener,
				MouseMotionListener {

		@Override
		public void mouseReleased(MouseEvent event) {
			mouseDown = false;
			myImage.getDrawActions().add(drawAction);
			drawAction = null;
			refresh();
		}

		@Override
		public void mousePressed(MouseEvent event) {
			mouseDown = true;
			drawAction = activeTool.getAction();
			drawAction.setColor(color);
			drawAction.addPoint(new Point(event.getX(), event.getY()));

			refresh();
		}

		@Override
		public void mouseExited(MouseEvent event) {
		}

		@Override
		public void mouseEntered(MouseEvent event) {
		}

		@Override
		public void mouseClicked(MouseEvent event) {
		}

		@Override
		public void mouseDragged(MouseEvent event) {
			drawAction.addPoint(new Point(event.getX(), event.getY()));
			refresh();
		}

		@Override
		public void mouseMoved(MouseEvent event) {
		}
	}
}
