package pl.mypaint.gui;

import pl.mypaint.actions.FlipRotateAction;
import pl.mypaint.tools.*;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;

public class ToolBar extends JPanel {
	private static final long serialVersionUID = 2624592366939220131L;

	private JButton btnColor = new JButton("Color");
	private JButton btnPencil = new JButton("Pencil");
	private JButton btnLine = new JButton("Line");
	private JButton btnCircle = new JButton("Circle");
	private JButton btnRect = new JButton("Rectangle");
	private JButton btnRubber = new JButton("Rubber");
	private JButton btnBucket = new JButton("Bucket");

	private JButton btnFlipVertical = new JButton("Flip Vertical");
	private JButton btnFlipHorizontal = new JButton("Flip Horizontal");

	private JButton btnRotate90 = new JButton("Rotate 90");
	private JButton btnRotate180 = new JButton("Rotate 180");
	private JButton btnRotate270 = new JButton("Rotate 270");

	private JTextField dspColor = new JTextField();

	private MyPaint parentFrame;
	private ImagePanel imagePanel;

	private List<JButton> activableButtons = new ArrayList<JButton>();

	public ToolBar(MyPaint parent) {
		this.parentFrame = parent;
		this.imagePanel = parentFrame.getImagePanel();
		this.setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
		this.setAlignmentX(LEFT_ALIGNMENT);

		addComponents();
		initComponents();

		this.addComponent(dspColor);
		this.dspColor.setEditable(false);
		this.dspColor.setBackground(imagePanel.getColor());
		setAsActive(btnPencil);
	}

	private void addComponents() {
		this.addComponent(btnPencil);
		this.addComponent(btnLine);
		this.addComponent(btnCircle);
		this.addComponent(btnRect);
		this.addComponent(btnRubber);
		this.addComponent(btnBucket);

		this.addComponent(btnFlipVertical);
		this.addComponent(btnFlipHorizontal);
		this.addComponent(btnRotate90);
		this.addComponent(btnRotate180);
		this.addComponent(btnRotate270);

		this.addComponent(btnColor);

		activableButtons.add(btnPencil);
		activableButtons.add(btnLine);
		activableButtons.add(btnCircle);
		activableButtons.add(btnRect);
		activableButtons.add(btnRubber);
		activableButtons.add(btnBucket);
	}

	private void initComponents() {
		btnPencil.addMouseListener(new PencilMouseListener());
		btnLine.addMouseListener(new LineMouseListener());
		btnCircle.addMouseListener(new CircleMouseListener());
		btnRect.addMouseListener(new RectMouseListener());
		btnRubber.addMouseListener(new RubberMouseListener());
		btnBucket.addMouseListener(new BucketMouseListener());
		btnFlipVertical.addMouseListener(new FlipVerticalMouseListener());
		btnFlipHorizontal.addMouseListener(new FlipHorizontalMouseListener());
		btnRotate90.addMouseListener(new Rotate90MouseListener());
		btnRotate180.addMouseListener(new Rotate180MouseListener());
		btnRotate270.addMouseListener(new Rotate270MouseListener());
		btnColor.addMouseListener(new ColorMouseListener());
	}

	private void addComponent(Component component) {
		component.setPreferredSize(new Dimension(150, 25));
		component.setMinimumSize(new Dimension(150, 25));
		component.setMaximumSize(new Dimension(150, 25));
		JPanel panel = new JPanel();

		panel.setPreferredSize(new Dimension(150, 26));
		panel.setMinimumSize(new Dimension(150, 26));
		panel.setMaximumSize(new Dimension(150, 26));

		panel.add(component);
		this.add(panel);
	}

	private void setAsActive(JButton buttonToSetActive) {
		for (JButton btn : activableButtons) {
			buttonToSetActive.setForeground(Color.BLACK);
			buttonToSetActive.setBackground(Color.WHITE);
			Border line = new LineBorder(Color.BLACK);
			Border margin = new EmptyBorder(5, 15, 5, 15);
			Border compound = new CompoundBorder(line, margin);
			buttonToSetActive.setBorder(compound);

			if (buttonToSetActive != btn) {
				btn.setForeground(btnColor.getForeground());
				btn.setBackground(btnColor.getBackground());
				btn.setBorder(btnColor.getBorder());
			}
		}
	}

	public void resetButtons() {
		setAsActive(btnPencil);
	}

	private void rotate(int value) {
		Tool tool = imagePanel.getActiveTool();

		imagePanel.setActiveTool(new Rotate());

		FlipRotateAction action = new FlipRotateAction(new Rotate(value));
		action.setValue(value);
		imagePanel.getMyImage().getDrawActions().add(action);
		imagePanel.refresh();
		imagePanel.setActiveTool(tool);
	}

	private class ColorMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			parentFrame.getImagePanel().setColor(
					JColorChooser.showDialog(null, "Change Color", null));
			dspColor.setBackground(imagePanel.getColor());
		}
	}

	private class Rotate270MouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			rotate(270);
		}
	}

	private class Rotate180MouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			rotate(180);
		}
	}

	private class Rotate90MouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			rotate(90);
		}
	}

	private class FlipHorizontalMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {

			Tool tool = imagePanel.getActiveTool();

			imagePanel.setActiveTool(new FlipVertical());
			imagePanel.getMyImage().getDrawActions()
					.add(new FlipRotateAction(new FlipHorizontal()));
			imagePanel.setActiveTool(tool);
			imagePanel.refresh();
		}
	}

	private class FlipVerticalMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			Tool tool = imagePanel.getActiveTool();

			imagePanel.setActiveTool(new FlipVertical());

			imagePanel.getMyImage().getDrawActions()
					.add(new FlipRotateAction(new FlipVertical()));
			imagePanel.setActiveTool(tool);
			imagePanel.refresh();
		}
	}

	private class BucketMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			imagePanel.setActiveTool(new Bucket());
			setAsActive(btnBucket);
		}
	}

	private class RubberMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			imagePanel.setActiveTool(new Rubber());
			setAsActive(btnRubber);
		}
	}

	private class RectMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			imagePanel.setActiveTool(new Rectangle());
			setAsActive(btnRect);
		}
	}

	private class CircleMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			imagePanel.setActiveTool(new Circle());
			setAsActive(btnCircle);
		}
	}

	private class LineMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			imagePanel.setActiveTool(new Line());
			setAsActive(btnLine);
		}
	}

	private class PencilMouseListener extends MouseAdapter {
		public void mouseClicked(MouseEvent e) {
			imagePanel.setActiveTool(new Pencil());
			setAsActive(btnPencil);
		}
	}
}
