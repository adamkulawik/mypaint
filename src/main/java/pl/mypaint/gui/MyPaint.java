package pl.mypaint.gui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneLayout;

import pl.mypaint.gui.menu.MyPaintMenu;

public class MyPaint extends JFrame {
	private static final long serialVersionUID = 3361236157581946667L;

	private MyPaintMenu myMenuBar = new MyPaintMenu(this);
	private ImagePanel imagePanel = new ImagePanel(this);
	private ToolBar toolBar = new ToolBar(this);
	private JScrollPane scrollPanel = new JScrollPane();

	public MyPaint() {
		this.setSize(new Dimension(1024, 768));
		this.setLayout(new BorderLayout());

		this.setJMenuBar(myMenuBar);

		this.add(scrollPanel, BorderLayout.CENTER);

		scrollPanel.setLayout(new ScrollPaneLayout());
		scrollPanel.getViewport().setLayout(null);
		scrollPanel.getViewport().add(imagePanel);
		scrollPanel.getViewport().setBackground(Color.GRAY);
		scrollPanel.setBackground(Color.GRAY);

		this.add(toolBar, BorderLayout.WEST);

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public ImagePanel getImagePanel() {
		return imagePanel;
	}

	public void setImagePanel(ImagePanel imagePanel) {
		this.imagePanel = imagePanel;
	}

	public void undo() {
		imagePanel.undo();
	}

	public ToolBar getToolBar() {
		return toolBar;
	}

	public void setToolBar(ToolBar toolBar) {
		this.toolBar = toolBar;
	}

	public MyPaintMenu getMyMenuBar() {
		return myMenuBar;
	}

	public void setMyMenuBar(MyPaintMenu myMenuBar) {
		this.myMenuBar = myMenuBar;
	}
}
