package pl.mypaint.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.KeyStroke;

import pl.mypaint.gui.MyPaint;

public class EditMenu extends JMenu {
	private static final long serialVersionUID = -3358851626447415255L;

	private MyPaint parent;
	private JMenuItem undoMenuItem = new JMenuItem("Undo");

	public EditMenu(MyPaint parent) {
		super("Edit");

		undoMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
				ActionEvent.CTRL_MASK));

		undoMenuItem.addActionListener(new undoMenuItemListener());

		this.add(undoMenuItem);
		this.parent = parent;
	}

	private class undoMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			parent.undo();
		}
	}

	public JMenuItem getUndoMenuItem() {
		return undoMenuItem;
	}

	public void setUndoMenuItem(JMenuItem undoMenuItem) {
		this.undoMenuItem = undoMenuItem;
	}
}
