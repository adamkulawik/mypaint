package pl.mypaint.gui.menu;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.filechooser.FileNameExtensionFilter;

import pl.mypaint.gui.DlgNewDrawing;
import pl.mypaint.gui.MyPaint;
import pl.mypaint.gui.MyPaintIO;

public class FileMenu extends JMenu {
	private File choosenFile;

	private static final long serialVersionUID = -6058243310705533086L;

	private MyPaint parent;

	private JMenuItem newMenuItem = new JMenuItem("New");
	private JMenuItem openMenuItem = new JMenuItem("Open");
	private JMenuItem saveMenuItem = new JMenuItem("Save");
	private JMenuItem saveAsMenuItem = new JMenuItem("Save as");

	public FileMenu(MyPaint parent) {
		super("File");

		newMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,
				ActionEvent.CTRL_MASK));
		openMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O,
				ActionEvent.CTRL_MASK));
		saveMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				ActionEvent.CTRL_MASK));
		saveAsMenuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S,
				ActionEvent.CTRL_MASK | ActionEvent.SHIFT_MASK));

		newMenuItem.addActionListener(new newMenuItemListener());
		openMenuItem.addActionListener(new openMenuItemListener());
		saveMenuItem.addActionListener(new saveMenuItemListener());
		saveAsMenuItem.addActionListener(new saveAsMenuItemListener());

		this.add(newMenuItem);
		this.add(openMenuItem);
		this.add(saveMenuItem);
		this.add(saveAsMenuItem);

		this.parent = parent;
	}

	public File getFileName(Operation operation) {
		JFileChooser fileChooser = new JFileChooser();
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);

		if ((operation == Operation.SAVE) && (choosenFile != null))
			fileChooser.setSelectedFile(choosenFile);

		FileNameExtensionFilter filter = new FileNameExtensionFilter("Bitmaps",
				"bmp", "BMP");

		fileChooser.setApproveButtonText(operation.toString());
		fileChooser.setDialogTitle(operation.toString() + " file");

		fileChooser.setFileFilter(filter);

		if (operation == Operation.OPEN)
			fileChooser.showOpenDialog(parent);
		else
			fileChooser.showSaveDialog(parent);

		File fileName = fileChooser.getSelectedFile();

		if (fileName != null) {
			if (!fileName.getName().toLowerCase().contains(".bmp")) {
				fileName = new File(fileName.getAbsolutePath() + ".bmp");
			}
			choosenFile = fileName;
		}
		return fileName;
	}

	private class newMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			DlgNewDrawing dialog = new DlgNewDrawing(parent.getImagePanel());
			dialog.setVisible(true);
		}
	}

	private class openMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {

			File file = getFileName(Operation.OPEN);

			if (file == null)
				return;

			try {
				parent.getImagePanel().setImage((MyPaintIO.readImage(file)));
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,
						"Error during reading a file.");
			}
		}
	}

	private class saveMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {

			File file;

			if (choosenFile == null) {

				file = getFileName(Operation.SAVE);

				if (file == null)
					return;
			} else {
				file = choosenFile;
			}

			try {
				MyPaintIO.writeImage(file, parent.getImagePanel()
						.getDisplayedImage());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,
						"Error during writing a file.");
			}
		}
	}

	private class saveAsMenuItemListener implements ActionListener {
		@Override
		public void actionPerformed(ActionEvent event) {
			File file = getFileName(Operation.SAVE_AS);

			if (file == null)
				return;

			try {
				MyPaintIO.writeImage(file, parent.getImagePanel()
						.getDisplayedImage());
			} catch (IOException e) {
				JOptionPane.showMessageDialog(null,
						"Error during writing a file.");
			}
		}
	}

	private enum Operation {
		SAVE("Save"), OPEN("Open"), SAVE_AS("Save");

		private String text;

		public String toString() {
			return text;
		}

		private Operation(String text) {
			this.text = text;
		};
	}
}
