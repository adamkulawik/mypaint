package pl.mypaint.gui.menu;

import javax.swing.JMenuBar;

import pl.mypaint.gui.MyPaint;

public class MyPaintMenu extends JMenuBar {
	private static final long serialVersionUID = -5759994254463067405L;

	public MyPaintMenu(MyPaint parent) {
		this.add(new FileMenu(parent));
		this.add(new EditMenu(parent));
	}
}
