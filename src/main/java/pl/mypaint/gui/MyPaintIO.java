package pl.mypaint.gui;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

public class MyPaintIO {

	public static void writeImage(File file, BufferedImage image)
			throws IOException {
		ImageIO.write(image, "BMP", file);
	};

	public static BufferedImage readImage(File file) throws IOException {
		return ImageIO.read(file);
	};
}
