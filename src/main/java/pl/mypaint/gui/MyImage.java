package pl.mypaint.gui;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.LinkedList;

import pl.mypaint.actions.DrawAction;

public class MyImage {
	private BufferedImage originalImage;

	private LinkedList<DrawAction> drawActions = new LinkedList<DrawAction>();

	private BufferedImage displayedImage;

	public MyImage(Dimension d) {
		originalImage = new BufferedImage(d.width, d.height,
				BufferedImage.TYPE_INT_RGB);
		displayedImage = new BufferedImage(d.width, d.height,
				BufferedImage.TYPE_INT_RGB);

		Graphics2D graphics = originalImage.createGraphics();

		graphics.setPaint(Color.WHITE);
		graphics.fillRect(0, 0, originalImage.getWidth(),
				originalImage.getHeight());
	}

	public BufferedImage getOriginalImage() {
		return originalImage;
	}

	public void setOriginalImage(BufferedImage image) {
		drawActions.clear();
		this.originalImage = image;
	}

	public void draw() {
		if (drawActions.size() > 0) {
			drawActions.get(drawActions.size() - 1).draw(this);
		}
	}

	public void redraw() {
		displayedImage = new BufferedImage(originalImage.getWidth(),
				originalImage.getHeight(), originalImage.getType());

		displayedImage.getGraphics().drawImage(originalImage, 0, 0, null);

		for (DrawAction action : drawActions) {
			action.draw(this);
		}
	}

	public LinkedList<DrawAction> getDrawActions() {
		return drawActions;
	}

	public void setDrawActions(LinkedList<DrawAction> drawActions) {
		this.drawActions = drawActions;
	}

	public void undo() {
		if (drawActions.size() > 0) {
			drawActions.removeLast();
		}
	}

	public boolean isUndoable() {

		if (drawActions.size() == 0) {
			return false;
		}

		return true;
	}

	public BufferedImage getDisplayedImage() {
		return displayedImage;
	}

	public void setDisplayedImage(BufferedImage displayedImage) {
		this.displayedImage = displayedImage;
	}
}
