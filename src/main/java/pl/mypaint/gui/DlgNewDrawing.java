package pl.mypaint.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.AbstractAction;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.text.AbstractDocument;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.DocumentFilter;

public class DlgNewDrawing extends JDialog {
	private static final long serialVersionUID = -2938403916461752064L;

	private JLabel lblHeight = new JLabel("Wysoko��");
	private JMyTextField txtHeight = new JMyTextField("768");
	private JLabel lblWidth = new JLabel("Szeroko��");
	private JMyTextField txtWidth = new JMyTextField("1024");

	private JButton btnOK = new JButton("OK");

	private JPanel okPanel = new JPanel();
	private JPanel inputPanel = new JPanel();
	private JPanel widthPanel = new JPanel();
	private JPanel heightPanel = new JPanel();

	private ImagePanel imagePanel;

	private void initDialog() {
		this.setModalityType(ModalityType.APPLICATION_MODAL);

		initInputPanel();
		initOkPanel();

		this.getRootPane().setDefaultButton(btnOK);

		this.setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));

		this.add(inputPanel);
		this.add(okPanel);

		this.setLocationRelativeTo(null);
		this.setPreferredSize(new Dimension(300, 150));
		this.pack();
	}

	private void initInputPanel() {
		inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));
		inputPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		inputPanel.setAlignmentY(Component.CENTER_ALIGNMENT);

		widthPanel.setLayout(new BoxLayout(widthPanel, BoxLayout.X_AXIS));
		heightPanel.setLayout(new BoxLayout(heightPanel, BoxLayout.X_AXIS));

		widthPanel.add(lblWidth);
		widthPanel.add(txtWidth);
		heightPanel.add(lblHeight);
		heightPanel.add(txtHeight);

		inputPanel.add(widthPanel);
		inputPanel.add(heightPanel);
	}

	private void initOkPanel() {
		okPanel.setLayout(new BoxLayout(okPanel, BoxLayout.X_AXIS));

		btnOK.setMnemonic(KeyEvent.VK_ENTER);

		btnOK.addMouseListener(new OKListener());

		btnOK.setFocusable(true);
		btnOK.getInputMap().put(KeyStroke.getKeyStroke("ENTER"), "enter");
		btnOK.getActionMap().put("enter", new BtnOkAction());

		okPanel.setAlignmentX(Component.RIGHT_ALIGNMENT);
		okPanel.add(btnOK);
	}

	public DlgNewDrawing(ImagePanel imagePanel) {

		initDialog();
		this.imagePanel = imagePanel;
	}

	public void okPressed() {
		if ((txtHeight.getText().length() == 0)
				|| (txtWidth.getText().length() == 0)) {
			JOptionPane.showMessageDialog(null,
					"Szeroko�� i wysoko�� musz� by� wprowadzone!.");
			return;
		}

		imagePanel.newImagePanel(new Dimension(Integer.parseInt(txtWidth
				.getText()), Integer.parseInt(txtHeight.getText())));
		dispose();
	}

	private class MyDocumentFilter extends DocumentFilter {
		@Override
		public void insertString(DocumentFilter.FilterBypass fp, int offset,
				String string, AttributeSet aset) throws BadLocationException {
			int len = string.length();
			boolean isValidInteger = true;

			for (int i = 0; i < len; i++) {
				if (!Character.isDigit(string.charAt(i))) {
					isValidInteger = false;
					break;
				}
			}

			if (isValidInteger)
				super.insertString(fp, offset, string, aset);
			else
				Toolkit.getDefaultToolkit().beep();
		}

		@Override
		public void replace(DocumentFilter.FilterBypass fp, int offset,
				int length, String string, AttributeSet aset)
				throws BadLocationException {
			int len = string.length();
			boolean isValidInteger = true;

			for (int i = 0; i < len; i++) {
				if (!Character.isDigit(string.charAt(i))) {
					isValidInteger = false;
					break;
				}
			}
			if (isValidInteger)
				super.replace(fp, offset, length, string, aset);
			else
				Toolkit.getDefaultToolkit().beep();
		}
	}

	private class OKListener extends MouseAdapter implements ActionListener {

		public void mouseClicked(MouseEvent e) {
			okPressed();
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			okPressed();
		}
	}

	private class JMyTextField extends JTextField {

		private static final long serialVersionUID = -6522805768538394216L;

		public JMyTextField(String text) {
			super(text);

			this.setMaximumSize(new Dimension(100, 25));
			this.addFocusListener(new MyFocusListener());

			((AbstractDocument) this.getDocument())
					.setDocumentFilter(new MyDocumentFilter());

		}

		private class MyFocusListener implements FocusListener {

			@Override
			public void focusLost(FocusEvent arg0) {
			}

			@Override
			public void focusGained(FocusEvent arg0) {
				JMyTextField.this.selectAll();
			}
		}
	}

	private class BtnOkAction extends AbstractAction {

		private static final long serialVersionUID = -2816560268531702714L;

		@Override
		public void actionPerformed(ActionEvent e) {
			okPressed();
		}
	}
}