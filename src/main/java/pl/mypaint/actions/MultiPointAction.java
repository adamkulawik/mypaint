package pl.mypaint.actions;

import java.awt.Point;
import pl.mypaint.tools.Tool;

public class MultiPointAction extends DrawAction {
	public MultiPointAction(Tool tool) {
		super(tool);
	}

	@Override
	public void addPoint(Point point) {
		points.add(point);
	}
}
