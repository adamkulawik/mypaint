package pl.mypaint.actions;

import java.awt.Point;

import pl.mypaint.tools.Tool;

public class SinglePointAction extends DrawAction {
	public SinglePointAction(Tool tool) {
		super(tool);
	}

	@Override
	public void addPoint(Point point) {
		if (points.size() == 0) {
			points.add(point);
		}
	}
}
