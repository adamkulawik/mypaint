package pl.mypaint.actions;

import pl.mypaint.gui.MyImage;
import pl.mypaint.tools.Tool;

import java.awt.Color;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public abstract class DrawAction {
	protected Color color;
	protected Tool tool;

	protected List<Point> points = new ArrayList<Point>();

	protected int value;

	public DrawAction(Tool tool) {
		this.tool = tool;
	}

	public final void draw(MyImage image) {
		tool.draw(image, points, color);
	};

	public abstract void addPoint(Point point);

	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}
}
