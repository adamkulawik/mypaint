package pl.mypaint.actions;

import java.awt.Point;

import pl.mypaint.tools.Tool;

public class TwoPointAction extends DrawAction {
	public TwoPointAction(Tool tool) {
		super(tool);
	}

	@Override
	public void addPoint(Point point) {
		if (points.size() < 2) {
			points.add(point);
		} else {
			points.set(1, point);
		}
	}
}
