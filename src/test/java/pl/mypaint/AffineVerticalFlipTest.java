package pl.mypaint;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Collection;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import pl.mypaint.affineOperations.AffineVerticalFlip;

@RunWith(value = Parameterized.class)
public class AffineVerticalFlipTest {
	private static BufferedImage imageForTest;

	private Point checkedPoint;
	private Dimension expectedDimension;
	private Color excectedCheckedPointColor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		imageForTest = new BufferedImage(10, 2, BufferedImage.TYPE_INT_RGB);

		Graphics2D graphics = imageForTest.createGraphics();

		graphics.setPaint(new Color(255, 255, 255));
		graphics.fillRect(0, 0, imageForTest.getWidth(),
				imageForTest.getHeight());

		int rgb = new Color(0, 0, 0).getRGB();
		imageForTest.setRGB(0, 0, rgb);
		imageForTest.setRGB(0, 1, rgb);
		imageForTest.setRGB(1, 0, rgb);
	}

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = new Object[][]{
				{new Point(0, 0), new Dimension(10, 2), new Color(0, 0, 0)},
				{new Point(0, 1), new Dimension(10, 2), new Color(0, 0, 0)},
				{new Point(1, 1), new Dimension(10, 2), new Color(0, 0, 0)},
				{new Point(1, 0), new Dimension(10, 2),
						new Color(255, 255, 255)},
				{new Point(9, 0), new Dimension(10, 2),
						new Color(255, 255, 255)},
				{new Point(9, 1), new Dimension(10, 2),
						new Color(255, 255, 255)},
				{new Point(8, 0), new Dimension(10, 2),
						new Color(255, 255, 255)}};

		return Arrays.asList(data);
	}

	public AffineVerticalFlipTest(Point checkedPoint,
			Dimension expectedDimension, Color excectedCheckedPointColor) {
		super();

		this.checkedPoint = checkedPoint;
		this.expectedDimension = expectedDimension;
		this.excectedCheckedPointColor = excectedCheckedPointColor;
	}

	@Test
	public void testFlipVerticalDimension() {
		BufferedImage flippedImage = AffineVerticalFlip
				.flipVertical(imageForTest);
		Dimension testedDimension = new Dimension(flippedImage.getWidth(),
				flippedImage.getHeight());
		Assert.assertEquals(expectedDimension, testedDimension);
	}

	@Test
	public void testFlipVerticalColor() {
		BufferedImage flippedImage = AffineVerticalFlip
				.flipVertical(imageForTest);
		Color testedColor = new Color(flippedImage.getRGB(checkedPoint.x,
				checkedPoint.y));
		Assert.assertEquals(excectedCheckedPointColor, testedColor);
	}

}
