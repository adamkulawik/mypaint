package pl.mypaint;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.util.Arrays;
import java.util.Collection;

import pl.mypaint.affineOperations.AffineRotation;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class AffineRotationTest {
	private static BufferedImage imageForTest;

	private int degree;
	private Point checkedPoint;
	private Dimension expectedDimension;
	private Color excectedCheckedPointColor;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		imageForTest = new BufferedImage(10, 2, BufferedImage.TYPE_INT_RGB);

		Graphics2D graphics = imageForTest.createGraphics();

		graphics.setPaint(new Color(255, 255, 255));
		graphics.fillRect(0, 0, imageForTest.getWidth(),
				imageForTest.getHeight());

		int rgb = new Color(0, 0, 0).getRGB();
		imageForTest.setRGB(0, 0, rgb);
		imageForTest.setRGB(0, 1, rgb);
		imageForTest.setRGB(1, 0, rgb);

	}

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = new Object[][]{
				{90, new Point(1, 0), new Dimension(2, 10), new Color(0, 0, 0)},
				{180, new Point(9, 1), new Dimension(10, 2), new Color(0, 0, 0)},
				{270, new Point(0, 9), new Dimension(2, 10), new Color(0, 0, 0)}};
		return Arrays.asList(data);
	}

	public AffineRotationTest(int degree, Point checkedPoint,
			Dimension expectedDimension, Color excectedCheckedPointColor) {
		super();
		this.degree = degree;
		this.checkedPoint = checkedPoint;
		this.expectedDimension = expectedDimension;
		this.excectedCheckedPointColor = excectedCheckedPointColor;
	}

	@Test
	public void testRotateImageDimension() {
		BufferedImage rotatedImage = AffineRotation.rotateImage(imageForTest,
				degree);
		Dimension testedDimension = new Dimension(rotatedImage.getWidth(),
				rotatedImage.getHeight());
		Assert.assertEquals(expectedDimension, testedDimension);
	}

	@Test
	public void testRotateImageColor() {
		BufferedImage rotatedImage = AffineRotation.rotateImage(imageForTest,
				degree);
		Color testedColor = new Color(rotatedImage.getRGB(checkedPoint.x,
				checkedPoint.y));
		Assert.assertEquals(excectedCheckedPointColor, testedColor);
	}
}
