package pl.mypaint;

import java.awt.Point;
import java.util.Arrays;
import java.util.Collection;

import pl.mypaint.tools.Circle;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

@RunWith(value = Parameterized.class)
public class CircleTest {
	Point center;
	Point pointOnCircle;
	int expectedResult;

	public CircleTest(Point center, Point pointOnCircle, int expectedResult) {
		super();
		this.center = center;
		this.pointOnCircle = pointOnCircle;
		this.expectedResult = expectedResult;
	}

	@Parameters
	public static Collection<Object[]> data() {
		Object[][] data = new Object[][]{
				{new Point(10, 10), new Point(10, 0), 10},
				{new Point(10, 10), new Point(10, 20), 10},
				{new Point(10, 10), new Point(10, 10), 0},
				{new Point(10, 10), new Point(20, 10), 10},
				{new Point(10, 10), new Point(5, 5), 7},};

		return Arrays.asList(data);
	}

	@Test
	public void testCountRadius() {
		int calculatedRadius = Circle.countRadius(center, pointOnCircle);
		Assert.assertEquals(expectedResult, calculatedRadius);
	}
}
